# MuSDaFA: Multi-Sensor Data Fusion with Accretion

This website documents experiments conducted in the emergent MuSDaFA research project at Zurich University of Applied Sciences, Switzerland.
MuSDaFA aims at improving the confidence in IoT, specifically in sensor data measurements and beacon signals, by exploiting redundant sensors and beacons. Time-synchronised
sensor data are compared and accreted into reliable measurement streams. As our society becomes increasingly automated based on sensed metrics, we expect
MuSDaFA to yield new insights into methods and approaches to avoid damage through faulty, unreliable or malicious sensors.

MuSDaFA uses [DiMOS](https://dimos-core.github.io), the Distributed Metrics Observation System, for some of the experiments.
The DiMOS format and tooling is compatible with IoT redundancy-based scenarios, such as MMR (Multi-Module Redundancy) and sensor fusion applications.

In the following, we report on a **Sensor Experiment** and a **Beacon Experiment**.

## Sensor Experiment

*Research Question: Can we automatically ignore (or correct) faulty or malicious sensors in a pool of N sensors?*

### Experiment Setup

This experiment is conducted with Phidgets hardware.
We designed a demo configuration of 5 light sensors on a networked hub, to simulate a scenario of 5 edge nodes with 1 sensor each, submitting a measurement.

![Sensors connected to hub photo](phidget_photo.jpg)

Here, the DiMOS DCC (Data-Centric Consensus) system plays the role of a traditional MMR Voter system, with the added benefits of a DiMOS-compatible modular data format for adding more sensors and nodes and a machine readable reliability report that provides insight on the trustworthiness of each sensor on each node as well as the overall trustworthiness of the output.

![DiMOS MMR configuration](dimos_iot.png)

As with standard MMR systems, unreliable sensors are detected and the output is adjusted accordingly. These outputs and reliability insights can in turn be used by other intelligent automation systems to mark sensors that need replacing, or to control the behaviours of other connected systems.

Moreover, to facilitate the rapid generation of experiment data, we created a portable MuSDaFA box with light sensor board, Raspberry Pi and Phidget hub. It was among other places showcased at the Swiss pavilion during Expo in Dubai.

![Integrated sensor board](musdafa-sensorboard.jpg)

### Results

Results including reference data will be reported around May 2022.

## Beacon Experiment

*Research Question: Can we improve the accuracy of triangulation and real-time location services by stacking N beacons?*

### Experiment Setup

Stacks of multiple BLE beacons each are placed with inter-distances of several meters. A person using a mobile device (e.g. Notebook running bluetoothctl)
walks in lines between the stack locations. Distances to beacons are determined through RSSI. As RSSI is a heuristic metric, averaging the RSSI for the
stack shall flatten sudden deviations and lead to more accurate signal strengths. This is done in a reliable way, also taking beacons into account
that are switched off or that do not report RSSI.

![Stack of beacons photo](beaconstacks.jpg)

### Results

A first experiment with two three-beacon stacks confirms the slowness of RSSI updates that only occur around every two seconds. In the graph below, the movement happens in between
the two blue vertical bars. The nearby and faraway stacks swap positions on the RSSI graph, but not immediately and also not in a very predictable
way.

![Inaccuracies in distance determination during movement](plotter.png)

A second experiment with three five-beacon stacks gives a more intuitive impression of the delays in receiving RSSI updates. In addition to potential changes in the client-side Bluetooth stack, there are physical constraints of simply not receiving a signal even across modest distances of few meters.

![Triangulation with beacon stacks](beaconstacks-small.webm)

A third experiment correlates beacon signal strength data with GPS coordinates. The data are plotted to a heatmap outlining areas with strong signals.

![Beacon signal heatmap](beaconheatmap-small.webm)

## Publications

To follow in mid to late 2022.
